import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import SettingsScreen from '../screens/SettingsScreen';
import DrawerContent from '../components/DrawerContent';
import HomeStack from './HomeStack';
import MessagesStack from './MessagesStack';
import SearchScreen from '../screens/SearchScreen';
import SwipeScreen from '../screens/SwipeScreen';
import ProfileScreen from '../screens/ProfileScreen';
import TabBar from '../components/BottomTab/TabBar';

const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();

const MainTabScreen = () => {
  return (
    <Tab.Navigator tabBar={(props) => <TabBar {...props} />}>
      <Tab.Screen name="Home" component={HomeStack} initialParams={{ icon: 'home' }} />
      <Tab.Screen name="Search" component={SearchScreen} initialParams={{ icon: 'search' }} />
      <Tab.Screen name="Swipe" component={SwipeScreen} initialParams={{ icon: 'add-circle' }} />
      <Tab.Screen
        name="Messages"
        component={MessagesStack}
        initialParams={{ icon: 'notifications' }}
      />
      <Tab.Screen name="Profile" component={ProfileScreen} initialParams={{ icon: 'person' }} />
    </Tab.Navigator>
  );
};

const AppStack = () => {
  return (
    <Drawer.Navigator drawerContent={(props) => <DrawerContent {...props} />}>
      <Drawer.Screen name="HomeDrawer" component={MainTabScreen} />
      <Drawer.Screen name="Settings" component={SettingsScreen} />
    </Drawer.Navigator>
  );
};

export default AppStack;
