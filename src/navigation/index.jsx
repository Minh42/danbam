import React from 'react';
import { AuthProvider } from '../contexts/AuthProvider';
import Routes from './Routes';

const RootNavigation = () => {
  return (
    <AuthProvider>
      <Routes />
    </AuthProvider>
  );
};

export default RootNavigation;
