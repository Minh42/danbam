import React, { useState, useEffect, useContext } from 'react';
import { AuthContext } from '../contexts/AuthProvider';
import firebase from '../services/firebase';
import AppStack from './AppStack';
import AuthStack from './AuthStack';
import { NavigationContainer } from '@react-navigation/native';
import ActivityIndicator from '../components/ActivityIndicator';
import { TabBarProvider } from '../contexts/TabBarProvider';
import { FloatingButtonProvider } from '../contexts/FloatingButtonProvider';

const Routes = () => {
  const { user, setUser } = useContext(AuthContext);
  const [initializing, setInitializing] = useState(true);

  const onAuthStateChanged = (user) => {
    if (user === null) setUser(null);
    if (user && user.emailVerified) setUser(user);
    if (initializing) setInitializing(false);
  };

  useEffect(() => {
    const subscriber = firebase.auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber;
  }, []);

  if (initializing) {
    return <ActivityIndicator />;
  }

  return (
    <FloatingButtonProvider>
      <TabBarProvider>
        <NavigationContainer>{user ? <AppStack /> : <AuthStack />}</NavigationContainer>
      </TabBarProvider>
    </FloatingButtonProvider>
  );
};

export default Routes;
