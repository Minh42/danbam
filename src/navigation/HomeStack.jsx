import React from 'react';
import HomeScreen from '../screens/HomeScreen';
import AddPostScreen from '../screens/AddPostScreen';
import { createStackNavigator } from '@react-navigation/stack';

const HomeStack = createStackNavigator();

const HomeStackNavigator = ({ navigation }) => {
  return (
    <HomeStack.Navigator headerMode="none">
      <HomeStack.Screen name="Home" component={HomeScreen} />
      <HomeStack.Screen name="AddPost" component={AddPostScreen} />
    </HomeStack.Navigator>
  );
};

export default HomeStackNavigator;
