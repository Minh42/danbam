import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ChatScreen from '../screens/ChatScreen';
import MessagesScreen from '../screens/MessagesScreen';
import NotificationsScreen from '../screens/NotificationsScreen';
import {
  primaryColor,
  whiteColor,
  blackColor,
  darkGreyColor2,
  primaryColorLight,
} from '../constants/Colors';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Icon from 'react-native-vector-icons/MaterialIcons';

const Tab = createMaterialTopTabNavigator();
const MessagesStack = createStackNavigator();

const TabsStackNavigator = () => {
  return (
    <Tab.Navigator
      style={{
        backgroundColor: whiteColor,
      }}
      tabBarOptions={{
        activeTintColor: whiteColor,
        inactiveTintColor: darkGreyColor2,
        style: {
          backgroundColor: primaryColorLight,
          borderRadius: 30,
          marginLeft: 16,
          marginRight: 16,
          marginTop: 8,
          marginBottom: 8,
        },
        indicatorStyle: {
          height: null,
          top: '0%',
          bottom: '0%',
          borderRadius: 100,
          backgroundColor: primaryColor,
        },
        labelStyle: {
          fontFamily: 'Montserrat-medium',
          fontSize: 15,
        },
      }}
    >
      <Tab.Screen name="Messages" component={MessagesScreen} />
      <Tab.Screen name="Notifications" component={NotificationsScreen} />
    </Tab.Navigator>
  );
};

const MessagesStackNavigator = () => {
  return (
    <MessagesStack.Navigator>
      <MessagesStack.Screen
        name="Messages"
        component={TabsStackNavigator}
        options={{
          title: 'Mes messages',
          headerTitleStyle: {
            color: blackColor,
            fontSize: 20,
          },
          headerRight: () => {
            return <Icon name="tune" size={27} style={{ marginRight: 16 }} color={blackColor} />;
          },
          headerTitleAlign: 'left',
          headerStyle: {
            shadowOpacity: 0,
            elevation: 0,
            backgroundColor: whiteColor,
          },
        }}
      />
      <MessagesStack.Screen
        name="Chat"
        component={ChatScreen}
        options={({ route }) => ({
          title: route.params.name,
          headerBackTitleVisible: false,
          headerRight: () => {
            return <Icon name="more-vert" size={27} style={{ marginRight: 16 }} />;
          },
        })}
      />
    </MessagesStack.Navigator>
  );
};

export default MessagesStackNavigator;
