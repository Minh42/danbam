import React from 'react';
import styled from 'styled-components';
import Post from './Post';
import { FlatList } from 'react-native';

const Container = styled(FlatList)`
  align-items: center;
  background-color: #f4f4f4;
`;

const Feed = () => {
  return (
    <Container>
      <Post />
    </Container>
  );
};

export default Feed;
