import React from 'react';
import styled from 'styled-components';
import UserInfo from './UserInfo';
import Img from '../assets/images/onboarding-img3.jpeg';
import { H4, H5, BodyTextRegularSmall, BodyTextRegularMedium } from '../atoms/Typography';
import { primaryColor, primaryColorLight, whiteColor } from '../constants/Colors';
import SocialAction from '../atoms/SocialAction';

const Container = styled.View`
    background-color: ${whiteColor};
    width: 100%;
    margin-bottom: 20px;
    shadow-color: ${primaryColor};
    shadow-offset: {width: 0, height: 8};
    shadow-opacity: 0.08;
    shadow-radius: 15px;
    elevation: 1;
`;

const PostHeader = styled.View`
  background-color: ${primaryColor};
  opacity: 0.8;
`;

const PostBody = styled.View`
  padding: 24px;
  border-bottom-width: 1px;
  border-bottom-color: ${primaryColorLight};
`;

const PostFooter = styled.View`
  padding: 24px;
`;

const Image = styled.ImageBackground`
  width: 100%;
  height: 220px;
`;

const OverlayText = styled.View`
  flex-direction: row;
  position: absolute;
  bottom: 16px;
  right: 16px;
`;

const PostInfo = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

const PostStats = styled.View`
  flex-direction: row;
  align-items: center;
`;

const Post = () => {
  return (
    <Container>
      <PostHeader>
        <Image source={Img}>
          <OverlayText>
            <H5 textColor={whiteColor}>Manga &nbsp;/&nbsp;</H5>
            <BodyTextRegularSmall textColor={whiteColor}>Manga japonais</BodyTextRegularSmall>
          </OverlayText>
        </Image>
      </PostHeader>
      <PostBody>
        <H4 style={{ marginBottom: 8 }}>Hello</H4>
        <BodyTextRegularMedium style={{ marginBottom: 16 }}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
          ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
          ullamco laboris nisi ut aliquip ex ea commodo consequat.
        </BodyTextRegularMedium>
        <PostInfo>
          <UserInfo size="medium" />
          <PostStats>
            <SocialAction name="favorite" count="3" hasMarginRight />
            <SocialAction name="comments" count="10" />
          </PostStats>
        </PostInfo>
      </PostBody>
      <PostFooter>
        <UserInfo size="small" />
      </PostFooter>
    </Container>
  );
};

export default Post;
