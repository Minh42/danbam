import React, { useContext } from 'react';
import { Formik } from 'formik';
import { View } from 'react-native';
import { forgotFormSchema } from '../utils/yup';
import { BodyTextRegularMedium, ErrorMessage } from '../atoms/Typography';
import InputEmail from './InputEmail';
import Button from '../atoms/Button';
import { AuthContext } from '../contexts/AuthProvider';

const ForgotForm = () => {
  const { forgotPassword } = useContext(AuthContext);
  return (
    <View>
      <Formik
        initialValues={{ email: '' }}
        validationSchema={forgotFormSchema}
        onSubmit={async (values, { setSubmitting, resetForm }) => {
          setSubmitting(true);
          await forgotPassword(values.email);
          setSubmitting(false);
        }}
      >
        {(props) => (
          <View>
            <BodyTextRegularMedium style={{ marginBottom: 4 }}>Email</BodyTextRegularMedium>
            <InputEmail props={props} />
            <ErrorMessage style={{ marginTop: 4, marginBottom: 8 }}>
              {props.touched.email && props.errors.email}
            </ErrorMessage>
            <Button
              text="Envoyer un lien"
              onPress={props.handleSubmit}
              disabled={props.isSubmitting}
            />
          </View>
        )}
      </Formik>
    </View>
  );
};

export default ForgotForm;
