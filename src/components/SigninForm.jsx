import React, { useContext } from 'react';
import { Formik } from 'formik';
import { View } from 'react-native';
import { signinFormSchema } from '../utils/yup';
import InputEmail from './InputEmail';
import InputPassword from './InputPassword';
import Button from '../atoms/Button';
import { BodyTextRegularMedium, BodyTextRegularSmall, ErrorMessage } from '../atoms/Typography';
import { AuthContext } from '../contexts/AuthProvider';

const SigninForm = ({ navigation }) => {
  const { signin } = useContext(AuthContext);
  return (
    <View>
      <Formik
        initialValues={{ email: '', password: '' }}
        validationSchema={signinFormSchema}
        onSubmit={async (values, { setSubmitting }) => {
          await signin(values.email, values.password);
          setSubmitting(false);
        }}
      >
        {(props) => (
          <View>
            <BodyTextRegularMedium style={{ marginBottom: 4 }}>Email</BodyTextRegularMedium>
            <InputEmail props={props} />
            <ErrorMessage style={{ marginTop: 4, marginBottom: 8 }}>
              {props.touched.email && props.errors.email}
            </ErrorMessage>
            <BodyTextRegularMedium style={{ marginBottom: 4 }}>Mot de passe</BodyTextRegularMedium>
            <InputPassword props={props} />
            <BodyTextRegularSmall
              style={{ marginTop: 8 }}
              onPress={() => navigation.navigate('Forgot')}
            >
              Mot de passe oublié ?
            </BodyTextRegularSmall>
            <ErrorMessage style={{ marginTop: 4, marginBottom: 16 }}>
              {props.touched.password && props.errors.password}
            </ErrorMessage>
            <Button
              text="Se connecter"
              onPress={props.handleSubmit}
              disabled={props.isSubmitting}
            />
          </View>
        )}
      </Formik>
    </View>
  );
};

export default SigninForm;
