import React from 'react';
import styled from 'styled-components';
import { BodyTextMediumSmall } from '../atoms/Typography';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

const Container = styled.View`
  position: absolute;
  bottom: 0px;
  height: auto;
  width: 100%;
  background-color: ${(props) => props.theme.primaryColor};
  align-items: center;
`;

const IconWrapper = styled.View`
  flex-direction: row;
`;

const Icon = styled(MaterialIcon)`
  color: ${(props) => props.theme.textColor.high};
`;

const IconAlt = styled(MaterialIcon)`
  color: ${(props) => props.theme.textColorAlt.high};
`;

const ActionIcon = styled.TouchableOpacity`
  height: 48px;
  width: 48px;
  justify-content: center;
  align-items: center;
  border-radius: ${(props) => props.theme.borderRadius.xxl};
  background-color: ${(props) => props.theme.surfaceColor};
`;

const UserActionsMenu = ({ navigation, closeBottomMenu }) => {
  const handleClose = (screen) => {
    closeBottomMenu();
    navigation.navigate(screen);
  };

  return (
    <Container>
      <BodyTextMediumSmall altColor="true" style={{ marginTop: 16, marginBottom: 16 }}>
        Poster sur Danbam
      </BodyTextMediumSmall>
      <IconWrapper>
        <ActionIcon style={{ marginRight: 24 }} onPress={() => handleClose('AddPost')}>
          <Icon name="ballot" size={27} />
        </ActionIcon>
        <ActionIcon style={{ marginRight: 24 }} onPress={() => handleClose('AddPost')}>
          <Icon name="ballot" size={27} />
        </ActionIcon>
        <ActionIcon onPress={() => handleClose('AddPost')}>
          <Icon name="ballot" size={27} />
        </ActionIcon>
      </IconWrapper>
      <IconAlt
        name="close"
        size={32}
        style={{ marginTop: 8, marginBottom: 16 }}
        onPress={closeBottomMenu}
      />
    </Container>
  );
};

export default UserActionsMenu;
