import React, { useState } from 'react';
import styled from 'styled-components';
import { Picker } from '@react-native-picker/picker';
import { primaryColor, whiteColor } from '../constants/Colors';

const Container = styled.View`
  flex: 1;
  justify-content: center;
`;

const Test = styled.View`
  background-color: ${whiteColor};
  width: 100%;
  height: 40%;
  position: absolute;
  bottom: 0;
`;

const TextPicker = () => {
  const [value, setValue] = useState(null);

  return (
    <Container>
      <Test>
        <Picker selectedValue={value} onValueChange={(itemValue) => setValue(itemValue)}>
          <Picker.Item label="Java" value="java" />
          <Picker.Item label="JavaScript" value="js" />
          <Picker.Item label="JavaScript2" value="js2" />
          <Picker.Item label="JavaScript2" value="js2" />
          <Picker.Item label="JavaScript2" value="js2" />
          <Picker.Item label="JavaScript2" value="js2" />
          <Picker.Item label="JavaScript2" value="js2" />
          <Picker.Item label="JavaScript2" value="js2" />
        </Picker>
      </Test>
    </Container>
  );
};

export default TextPicker;
