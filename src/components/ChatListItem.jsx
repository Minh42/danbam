import React from 'react';
import styled from 'styled-components';
import Avatar from '../atoms/Avatar';
import {
  H4,
  BodyTextRegularSmall,
  BodyTextRegularExtraSmall,
  BodyTextRegularMedium,
} from '../atoms/Typography';
import Img from '../assets/images/onboarding-img2.jpeg';
import moment from 'moment';

const Container = styled.TouchableOpacity`
  align-items: center;
  flex-direction: row;
  padding-left: 16px;
  padding-right: 16px;
  padding-top: 16px;
`;

const MessageWrapper = styled.View`
  flex: 1;
  flex-direction: column;
`;

const TextWrapper = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const ChatListItem = ({ navigation, item }) => {
  return (
    <Container onPress={() => navigation.navigate('Chat', { id: item._id, name: item.name })}>
      <Avatar source={Img} medium style={{ marginRight: 16 }} />
      <MessageWrapper>
        <TextWrapper>
          <H4>{item.name}</H4>
          <BodyTextRegularExtraSmall>
            {moment(item.lastMessage.createdAt.toDate()).fromNow()}
          </BodyTextRegularExtraSmall>
        </TextWrapper>
        <TextWrapper style={{ marginTop: 4 }}>
          <BodyTextRegularMedium numberOfLines={1}>
            {item.lastMessage.messageText}
          </BodyTextRegularMedium>
          <BodyTextRegularSmall>9</BodyTextRegularSmall>
        </TextWrapper>
      </MessageWrapper>
    </Container>
  );
};

export default ChatListItem;
