import React, { useContext } from 'react';
import { Formik } from 'formik';
import { View } from 'react-native';
import { signupFormSchema } from '../utils/yup';
import { BodyTextRegularMedium, ErrorMessage } from '../atoms/Typography';
import InputEmail from './InputEmail';
import InputPassword from './InputPassword';
import Button from '../atoms/Button';
import { AuthContext } from '../contexts/AuthProvider';

const SignupForm = () => {
  const { signup } = useContext(AuthContext);
  return (
    <View>
      <Formik
        initialValues={{ email: '', password: '' }}
        validationSchema={signupFormSchema}
        onSubmit={async (values, { setSubmitting, resetForm }) => {
          setSubmitting(true);
          await signup(values.email, values.password);
          setSubmitting(false);
        }}
      >
        {(props) => (
          <View>
            <BodyTextRegularMedium style={{ marginBottom: 4 }}>Email</BodyTextRegularMedium>
            <InputEmail props={props} />
            <ErrorMessage style={{ marginTop: 4, marginBottom: 8 }}>
              {props.touched.email && props.errors.email}
            </ErrorMessage>
            <BodyTextRegularMedium style={{ marginBottom: 4 }}>Mot de passe</BodyTextRegularMedium>
            <InputPassword props={props} />
            <ErrorMessage style={{ marginTop: 4, marginBottom: 16 }}>
              {props.touched.password && props.errors.password}
            </ErrorMessage>
            <Button
              text="Créer un compte"
              onPress={props.handleSubmit}
              disabled={props.isSubmitting}
            />
          </View>
        )}
      </Formik>
    </View>
  );
};

export default SignupForm;
