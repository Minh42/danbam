import React, { useContext } from 'react';
import styled from 'styled-components';
import { AuthContext } from '../contexts/AuthProvider';
import Facebook from '../assets/icons/facebook.png';
import Instagram from '../assets/icons/instagram.png';
import Twitter from '../assets/icons/twitter.png';
import { TouchableOpacity } from 'react-native';

const Container = styled.View`
  flex-direction: row;
  justify-content: center;
`;

const Image = styled.Image`
  height: 48px;
  width: 48px;
`;

const SocialOauth = () => {
  const { signinWithFacebook, signinWithInstagram, signinWithTwitter } = useContext(AuthContext);
  return (
    <Container>
      <TouchableOpacity onPress={() => signinWithFacebook()}>
        <Image source={Facebook} style={{ marginRight: 32 }} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => signinWithInstagram()}>
        <Image source={Instagram} style={{ marginRight: 32 }} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => signinWithTwitter()}>
        <Image source={Twitter} />
      </TouchableOpacity>
    </Container>
  );
};

export default SocialOauth;
