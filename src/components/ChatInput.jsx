import React from 'react';
import styled from 'styled-components';
import { BodyTextRegularExtraSmall } from '../atoms/Typography';

const Container = styled.View`
  flex-direction: row;
`;

const ChatInput = () => {
  return (
    <Container>
      <BodyTextRegularExtraSmall>Écrire un message</BodyTextRegularExtraSmall>
    </Container>
  );
};

export default ChatInput;
