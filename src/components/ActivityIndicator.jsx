import React from 'react';
import styled from 'styled-components';
import Img from '../assets/finger-heart.gif';

const Container = styled.View`
  flex: 1;
  background-color: ${(props) => props.theme.backgroundColor};
  justify-content: center;
  align-items: center;
`;

const Image = styled.Image`
  height: 108px;
  width: 108px;
`;

const ActivityIndicator = () => {
  return (
    <Container>
      <Image source={Img} />
    </Container>
  );
};

export default ActivityIndicator;
