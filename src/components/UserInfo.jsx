import React from "react";
import styled from "styled-components";
import Avatar from "../atoms/Avatar";
import Img from "../assets/images/onboarding-img4.jpeg";
import { H5, BodyTextRegularExtraSmall } from "../atoms/Typography";

const Container = styled.View`
  flex-direction: row;
  justify-content: flex-start;
`;

const UserInfoText = styled.View`
  flex-direction: column;
  justify-content: center;
  margin-left: 8px;
`;

const UserInfo = ({ size }) => {
  switch (size) {
    case "large":
      return (
        <Container>
          <Avatar source={Img} large />
          <UserInfoText>
            <H5>Minh Pham</H5>
            <BodyTextRegularExtraSmall>7 hours ago</BodyTextRegularExtraSmall>
          </UserInfoText>
        </Container>
      );
    case "medium":
      return (
        <Container>
          <Avatar source={Img} medium />
          <UserInfoText>
            <H5>Minh Pham</H5>
            <BodyTextRegularExtraSmall>7 hours ago</BodyTextRegularExtraSmall>
          </UserInfoText>
        </Container>
      );
    case "small":
      return (
        <Container>
          <Avatar source={Img} small />
          <UserInfoText>
            <H5>Minh Pham</H5>
            <BodyTextRegularExtraSmall>7 hours ago</BodyTextRegularExtraSmall>
          </UserInfoText>
        </Container>
      );
    default:
      return (
        <Container>
          <Avatar source={Img} small />
          <UserInfoText>
            <H5>Minh Pham</H5>
            <BodyTextRegularExtraSmall>7 hours ago</BodyTextRegularExtraSmall>
          </UserInfoText>
        </Container>
      );
  }
};

export default UserInfo;
