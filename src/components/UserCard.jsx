import React from 'react';
import styled from 'styled-components';
import { H3, H5, BodyTextRegularSmall } from '../atoms/Typography';
import {
  primaryColor,
  secondaryColor,
  whiteColor,
  orangeColor,
  primaryColorLight,
  yellowColor,
  turqoiseColor,
} from '../constants/Colors';
import TagWithIcon from '../atoms/TagWithIcon';
import Img from '../assets/images/onboarding-img4.jpeg';
import Tag from '../atoms/Tag';

const Container = styled.View`
  padding-left: 42px;
  padding-right: 42px;
  shadow-color: ${primaryColor};
  shadow-offset: {
    width: 0;
    height: 6;
  }
  shadow-opacity: 0.37;
  shadow-radius: 7.49;
  elevation: 12;
`;

const CardHeader = styled.View``;

const ImageBackground = styled.ImageBackground`
  height: 250px;
  width: auto;
`;

const CardBody = styled.View`
  height: auto;
  background-color: ${whiteColor};
  padding-left: 42px;
  padding right: 42px;
  padding-top: 12px;
  border-bottom-left-radius: 20px;
  border-bottom-right-radius: 20px;
`;

const Nationality = styled.View`
  padding-left: 16px;
  padding-right: 12px;
  height: 36px;
  position: absolute;
  bottom: 0;
  right: 0;
  border-top-left-radius: 20px;
  background-color: ${whiteColor};
  flex-direction: row;
  align-items: flex-end;
  justify-content: center;
`;

const TagWrapper = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
`;

const Divider = styled.View`
  border: 0.5px;
  border-color: ${primaryColorLight};
  margin-top: 16px;
  margin-bottom: 24px;
`;

const UserCard = ({ item, index, currentIndex }) => {
  return (
    <Container index={index} currentIndex={currentIndex}>
      <CardHeader>
        <ImageBackground
          source={Img}
          imageStyle={{ borderTopLeftRadius: 20, borderTopRightRadius: 20 }}
        >
          <TagWithIcon name="bolt" text="New" color={turqoiseColor} />
          <Nationality>
            <Tag color={primaryColor} style={{ marginRight: 14 }}>
              <BodyTextRegularSmall textColor={whiteColor}>Vietnamien</BodyTextRegularSmall>
            </Tag>
            <Tag color={orangeColor}>
              <BodyTextRegularSmall textColor={whiteColor}>Français</BodyTextRegularSmall>
            </Tag>
          </Nationality>
        </ImageBackground>
      </CardHeader>
      <CardBody>
        <H3>{item.userName}</H3>
        <BodyTextRegularSmall>18 ans, Paris 🇫🇷</BodyTextRegularSmall>
        <BodyTextRegularSmall textColor={primaryColor} style={{ marginTop: 20 }}>
          #mangas #shibainu #voyages #dramacoréens
        </BodyTextRegularSmall>
        <Divider />
        <H5 style={{ marginBottom: 24 }}>Ce que je recherche</H5>
        <TagWrapper>
          <Tag color={orangeColor} style={{ marginRight: 14, marginBottom: 20 }}>
            <BodyTextRegularSmall textColor={whiteColor}>
              Rencontrer des ami.es
            </BodyTextRegularSmall>
          </Tag>
          <Tag color={yellowColor} style={{ marginRight: 14, marginBottom: 20 }}>
            <BodyTextRegularSmall textColor={whiteColor}>
              Quelqu'un pour voyager
            </BodyTextRegularSmall>
          </Tag>
          <Tag color={secondaryColor}>
            <BodyTextRegularSmall textColor={whiteColor}>
              Conseils pour un voyage
            </BodyTextRegularSmall>
          </Tag>
        </TagWrapper>
      </CardBody>
    </Container>
  );
};

export default UserCard;
