import React, { useState } from 'react';
import styled from 'styled-components';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { TextInput } from '../atoms/Typography';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

const Container = styled.View`
  flex-direction: row;
  align-items: center;
  border-width: 1px;
  border-color: ${(props) => props.theme.borderColor};
  border-radius: ${(props) => props.theme.borderRadius.sm};
  padding-vertical: ${(props) => props.theme.spacing.sm};
  padding-horizontal: ${(props) => props.theme.spacing.md};
  justify-content: space-between;
`;

const Icon = styled(MaterialIcon)`
  color: ${(props) => props.theme.textColor.high};
`;

const TextInputWithIcon = styled.View`
  flex-direction: row;
  align-items: center;
`;

const InputPassword = ({ props }) => {
  const { handleChange, handleBlur, values } = props;
  const [secureTextEntry, setSecureTextEntry] = useState(true);

  const togglePassword = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  return (
    <Container>
      <TextInputWithIcon>
        <Icon name="lock" size={24} style={{ marginRight: 16 }} />
        <TextInput
          placeholder="Password"
          autoCapitalize="none"
          secureTextEntry={!!secureTextEntry}
          onChangeText={handleChange('password')}
          value={values.password}
          onBlur={handleBlur('password')}
        />
      </TextInputWithIcon>
      <TouchableOpacity onPress={togglePassword}>
        {secureTextEntry ? (
          <Icon name="visibility" size={24} />
        ) : (
          <Icon name="visibility-off" size={24} />
        )}
      </TouchableOpacity>
    </Container>
  );
};

export default InputPassword;
