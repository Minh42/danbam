import React from 'react';
import styled from 'styled-components';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

const Container = styled.View`
  width: 100%;
  height: 64px;
  border-top-width: 1px;
  border-top-color: ${(props) => props.theme.primaryLightColor};
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  background-color: ${(props) => props.theme.surfaceColor};
`;

const IconWrapper = styled.View`
  flex-direction: row;
`;

const Icon = styled(MaterialIcon)`
  color: ${(props) => props.theme.textColorAlt.high};
`;

const ActionIcon = styled.TouchableOpacity`
  height: 32px;
  width: 32px;
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.theme.backgroundColor};
  border-radius: ${(props) => props.theme.borderRadius.xxl};
`;

const PostActionsMenu = ({ takePhotoFromCamera, choosePhotoFromLibrary, addPost }) => {
  return (
    <Container>
      <IconWrapper>
        <ActionIcon style={{ marginRight: 8, marginLeft: 16 }}>
          <Icon name="photo-library" size={18} onPress={async () => choosePhotoFromLibrary()} />
        </ActionIcon>
        <ActionIcon style={{ marginRight: 8 }}>
          <Icon name="video-library" size={18} onPress={() => choosePhotoFromLibrary()} />
        </ActionIcon>
        <ActionIcon>
          <Icon name="gif" size={18} />
        </ActionIcon>
      </IconWrapper>
    </Container>
  );
};

export default PostActionsMenu;
