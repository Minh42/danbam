import React, { useEffect, useRef, useState } from 'react';
import { View, Animated, PanResponder, StyleSheet, LayoutAnimation, UIManager } from 'react-native';
import Layout from '../constants/Layout';
import UserCard from '../components/UserCard';

const { width } = Layout.window;
const SWIPE_THRESHOLD = 0.4 * width;
const SWIPE_DURATION = 250;

if (Platform.OS === 'android' && UIManager.setLayoutAnimationEnabledExperimental) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

const Deck = ({ data }) => {
  const [currentIndex, setCurrendIndex] = useState(0);

  const position = useRef(new Animated.ValueXY()).current;

  useEffect(() => {
    setCurrendIndex(0);
  }, [data]);

  const resetPosition = () => {
    Animated.spring(position, {
      toValue: {
        x: 0,
        y: 0,
      },
      useNativeDriver: false,
    }).start();
  };

  const onSwipeLeft = () => {};
  const onSwipeRight = () => {};

  const onForceSwipeComplete = (direction) => {
    const item = data[currentIndex];
    direction === 'left' ? onSwipeLeft(item) : onSwipeRight(item);
    position.setValue({ x: 0, y: 0 });
    setCurrendIndex((prevIndex) => prevIndex + 1);
  };

  const forceSwipe = (direction) => {
    const to_x = direction === 'right' ? width : -width;

    Animated.timing(position, {
      toValue: {
        x: to_x,
        y: 0,
      },
      duration: SWIPE_DURATION,
      useNativeDriver: false,
    }).start(({ finished }) => {
      if (finished) {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
        onForceSwipeComplete(direction);
      }
    });
  };

  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gesture) => {
        position.setValue({ x: gesture.dx, y: gesture.dy });
      },
      onPanResponderRelease: (event, gesture) => {
        if (gesture.dx > SWIPE_THRESHOLD) {
          forceSwipe('right');
        } else if (gesture.dx < -SWIPE_THRESHOLD) {
          forceSwipe('left');
        } else {
          resetPosition();
        }
      },
    }),
  ).current;

  const getCardStyle = () => {
    const rotate = position.x.interpolate({
      inputRange: [-width * 1.5, 0, width * 1.5],
      outputRange: ['-120deg', '0deg', '120deg'],
    });

    return {
      ...position.getLayout(),
      transform: [{ rotate }],
    };
  };

  const renderCards = () => {
    if (currentIndex >= data.length) {
      return (
        <View>
          <Text>No more Cards</Text>
        </View>
      );
    }
    return data
      .map((item, index) => {
        if (index === currentIndex) {
          return (
            <Animated.View
              key={item.id}
              style={[getCardStyle(), styles.card, { zIndex: 99 }]}
              {...panResponder.panHandlers}
            >
              <UserCard item={item} index={index} currentIndex={currentIndex} />
            </Animated.View>
          );
        } else if (index > currentIndex) {
          return (
            <Animated.View
              key={item.id}
              style={[styles.card, { top: 10 * (index - currentIndex), zIndex: 5 }]}
            >
              <UserCard item={item} index={index} currentIndex={currentIndex} />
            </Animated.View>
          );
        } else {
          return null;
        }
      })
      .reverse();
  };

  return <View>{renderCards()}</View>;
};

const styles = StyleSheet.create({
  card: {
    position: 'absolute',
    width,
  },
});

export default Deck;
