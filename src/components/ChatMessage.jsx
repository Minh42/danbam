import React, { useContext } from 'react';
import styled from 'styled-components';
import { BodyTextRegularSmall, BodyTextRegularExtraSmall } from '../atoms/Typography';
import { primaryColorLight, whiteColor, primaryColor } from '../constants/Colors';
import moment from 'moment';
import { AuthContext } from '../contexts/AuthProvider';

const Container = styled.View`
  padding: 10px;
`;

const MessageBox = styled.View`
  background-color: ${(props) => (props.backgroundColor ? primaryColorLight : whiteColor)}
  margin-left: ${(props) => (props.backgroundColor ? '50px' : '0px')}
  margin-right: ${(props) => (props.backgroundColor ? '0px' : '50px')}
  border-radius: 5px;
  padding: 10px;
`;

const ChatMessage = ({ item }) => {
  const { user } = useContext(AuthContext);

  const isMyMessage = () => {
    return item.user._id === user.uid;
  };

  return (
    <Container>
      <MessageBox backgroundColor={isMyMessage()}>
        {!isMyMessage() && <BodyTextRegularSmall>Minh Pham</BodyTextRegularSmall>}
        <BodyTextRegularSmall>{item.text}</BodyTextRegularSmall>
        <BodyTextRegularExtraSmall>{moment(item.createdAt).fromNow()}</BodyTextRegularExtraSmall>
      </MessageBox>
    </Container>
  );
};

export default ChatMessage;
