import React from 'react';
import styled from 'styled-components';
import * as Animatable from 'react-native-animatable';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { TextInput } from '../atoms/Typography';

const Container = styled.View`
  flex-direction: row;
  align-items: center;
  border-width: 1px;
  border-color: ${(props) => props.theme.borderColor};
  border-radius: ${(props) => props.theme.borderRadius.sm};
  padding-vertical: ${(props) => props.theme.spacing.sm};
  padding-horizontal: ${(props) => props.theme.spacing.md};
  justify-content: space-between;
`;

const Icon = styled(MaterialIcon)`
  color: ${(props) => props.theme.textColor.high};
`;

const TextInputWithIcon = styled.View`
  flex-direction: row;
  align-items: center;
`;

const InputEmail = ({ props }) => {
  const { handleChange, handleBlur, values, errors } = props;

  return (
    <Container>
      <TextInputWithIcon>
        <Icon name="person" size={24} style={{ marginRight: 16 }} />
        <TextInput
          placeholder="Email"
          autoCapitalize="none"
          secureTextEntry={false}
          onChangeText={handleChange('email')}
          value={values.email}
          onBlur={handleBlur('email')}
        />
      </TextInputWithIcon>
      {errors.email !== undefined ? null : (
        <Animatable.View animation="bounceIn" duration={1500}>
          <Icon name="done" size={24} />
        </Animatable.View>
      )}
    </Container>
  );
};

export default InputEmail;
