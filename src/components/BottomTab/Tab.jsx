import React, { useContext } from 'react';
import styled from 'styled-components';
import { TouchableOpacity } from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { AuthContext } from '../../contexts/AuthProvider';

const Icon = styled(MaterialIcon)`
  color: ${(props) =>
    props.route.name === props.selected ? props.theme.primaryColor : props.theme.primaryLightColor};
`;

const CurrentUser = styled.Image`
  height: 32px;
  width: 32px;
  border: 1px;
  border-radius: ${(props) => props.theme.borderRadius.sm};
  border-color: ${(props) =>
    props.route.name === props.selected ? props.theme.primaryColor : props.theme.primaryLightColor};
`;

const Tab = ({ icon, route, selected, onPress }) => {
  const { user } = useContext(AuthContext);
  switch (route.name) {
    case 'Swipe':
      return (
        <TouchableOpacity onPress={onPress}>
          <Icon name={icon} route={route} selected={selected} size={32} />
        </TouchableOpacity>
      );
    case 'Profile':
      return (
        <TouchableOpacity onPress={onPress}>
          <CurrentUser
            route={route}
            selected={selected}
            source={{
              uri: user.userImg,
            }}
          />
        </TouchableOpacity>
      );
    default:
      return (
        <TouchableOpacity onPress={onPress}>
          <Icon name={icon} route={route} selected={selected} size={27} />
        </TouchableOpacity>
      );
  }
};

export default Tab;
