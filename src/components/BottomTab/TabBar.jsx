import React, { useEffect, useState, useRef, useContext } from 'react';
import styled from 'styled-components';
import Tab from './Tab';
import Layout from '../../constants/Layout';
import { Animated } from 'react-native';
import { TabBarContext } from '../../contexts/TabBarProvider';

const Container = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  position: absolute;
  bottom: 0;
  height: 80px;
  width: ${Layout.window.width}px;
  background-color: ${(props) => props.theme.surfaceColor};
  padding-left: ${(props) => props.theme.spacing.xl};
  padding-right: ${(props) => props.theme.spacing.xl};
`;

const TabBar = ({ state, navigation }) => {
  const [selected, setSelected] = useState('Home');
  const { showTabBar } = useContext(TabBarContext);
  const animation = useRef(new Animated.Value(0)).current;

  const handlePress = (activeTab, index) => {
    if (state.index !== index) {
      setSelected(activeTab);
      navigation.navigate(activeTab);
    }
  };

  const toggleTabBar = () => {
    if (showTabBar) {
      Animated.timing(animation, {
        toValue: 0,
        duration: 200,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(animation, {
        toValue: 100,
        duration: 200,
        useNativeDriver: true,
      }).start();
    }
  };

  useEffect(() => {
    toggleTabBar();
  }, [showTabBar]);

  return (
    <Container as={Animated.View} style={{ transform: [{ translateY: animation }] }}>
      {state.routes.map((route, index) => (
        <Tab
          key={route.key}
          route={route}
          icon={route.params.icon}
          selected={selected}
          onPress={() => handlePress(route.name, index)}
        />
      ))}
    </Container>
  );
};

export default TabBar;
