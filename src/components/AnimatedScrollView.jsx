import React, { useContext } from 'react';
import styled from 'styled-components';
import { TabBarContext } from '../contexts/TabBarProvider';
import { FloatingButtonContext } from '../contexts/FloatingButtonProvider';

const ScrollView = styled.ScrollView`
  flex: 1;
  background-color: ${(props) => props.theme.backgroundColor};
  background-color: gray;
`;

let offsetY = 0;
const AnimatedScrollView = ({ children }) => {
  const { setShowTabBar } = useContext(TabBarContext);
  const { setShowFloatingButton } = useContext(FloatingButtonContext);
  return (
    <ScrollView
      scrollEventThrottle={16}
      onScroll={({ nativeEvent }) => {
        const newOffsetY = nativeEvent.contentOffset.y;
        if (newOffsetY <= 0) {
          setShowTabBar(true);
          setShowFloatingButton(false);
          return;
        }
        offsetY < newOffsetY ? setShowTabBar(false) : setShowTabBar(true);
        offsetY < newOffsetY ? setShowFloatingButton(true) : setShowFloatingButton(false);
        offsetY = newOffsetY;
      }}
    >
      {children}
    </ScrollView>
  );
};

export default AnimatedScrollView;
