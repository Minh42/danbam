import React, { useContext } from 'react';
import { Button } from 'react-native';
import styled from 'styled-components';
import { AuthContext } from '../contexts/AuthProvider';
import { useTheme } from '../contexts/ThemeProvider';
import { Switch } from 'react-native';
import { H1 } from '../atoms/Typography';
import baseTheme from '../themes/baseTheme';

const Container = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

const TextContainer = styled.View`
  width: 50px;
  height: 30px;
  background: ${(props) => props.theme.backgroundColor};
`;

const DrawerContent = () => {
  const { signout } = useContext(AuthContext);
  const theme = useTheme();

  return (
    <Container>
      <TextContainer>
        <H1 textColor={baseTheme.colors.white}>Bienvenue !</H1>
      </TextContainer>
      <Button title="Se déconnecter" onPress={() => signout()} />
      <Switch
        value={theme.mode === 'dark'}
        onValueChange={(value) => theme.setMode(value ? 'dark' : 'light')}
      />
    </Container>
  );
};

export default DrawerContent;
