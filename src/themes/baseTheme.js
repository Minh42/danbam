import { Dimensions } from 'react-native';

const { width } = Dimensions.get('window');
const { height } = Dimensions.get('window');

const baseTheme = {
  colors: {
    secondary: '#FF00FF',
    red: '#FB3F07',
    green: '#17D38A',
    purple: '#8338EC',
    turqoise: '#17FFD8',
    yellow: '#FFBE0B',
    orange: '#FB5607',
    white: '#FFFFFF',
    black: '#24262F',
    darkGrey1: '#999',
    darkGrey2: '#777',
    darkGrey3: '#333',
    lightGrey1: '#faf9f9',
    lightGrey2: '#f4f2f2',
    lightGrey3: '#f0eeee',
  },
  window: {
    width,
    height,
  },
  typography: {
    bold: 'Montserrat-bold',
    medium: 'Montserrat-medium',
    regular: 'Montserrat-regular',
  },
  borderColor: '#E5E8FF',
  borderRadius: {
    xs: '4px',
    sm: '8px',
    md: '16px',
    lg: '24px',
    xl: '32px',
    xxl: '48px',
  },
  spacing: {
    xs: '4px',
    sm: '8px',
    md: '16px',
    lg: '24px',
    xl: '32px',
    xxl: '48px',
  },
};

export default baseTheme;
