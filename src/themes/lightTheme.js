import baseTheme from './baseTheme';

const lightTheme = {
  theme: {
    ...baseTheme,
    backgroundColor: '#0000FF',
    surfaceColor: '#FFFFFF',
    primaryColor: '#0000FF',
    primaryLightColor: '#E5E8FF',
    textColor: {
      high: '#24262F',
      medium: '#24262F',
      low: '#24262F',
      disabled: '#24262F',
    },
    textColorAlt: {
      high: '#FFFFFF',
      medium: 'rgba(255, 255, 255, 0.87)',
      low: 'rgba(255, 255, 255, 0.6)',
      disabled: 'rgba(255, 255, 255, 0.38)',
    },
    error: '#FB3F07',
    success: '#17D38A',
    underlinedColor: '#24262F',
  },
};

export default lightTheme;
