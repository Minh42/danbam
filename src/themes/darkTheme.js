import baseTheme from './baseTheme';

const darkTheme = {
  theme: {
    ...baseTheme,
    backgroundColor: '#121212',
    surfaceColor: 'rgba(36, 38, 47, 0.8)',
    primaryColor: '#E5E8FF',
    primaryLightColor: 'rgba(255, 255, 255, 0.38)',
    textColor: {
      high: '#FFFFFF',
      medium: 'rgba(255, 255, 255, 0.87)',
      low: 'rgba(255, 255, 255, 0.6)',
      disabled: 'rgba(255, 255, 255, 0.38)',
    },
    textColorAlt: {
      high: '#24262F',
      medium: '#24262F',
      low: '#24262F',
      disabled: '#24262F',
    },
    error: 'rgba(251, 63, 7, 0.87)',
    success: 'rgba(23, 211, 138, 0.87)',
    underlinedColor: '#E5E8FF',
  },
};

export default darkTheme;
