import React from 'react';
import { AppearanceProvider } from 'react-native-appearance';
import { ManageThemeProvider } from '../contexts/ThemeProvider';

const ThemeManager = ({ children }) => (
  <AppearanceProvider>
    <ManageThemeProvider>{children}</ManageThemeProvider>
  </AppearanceProvider>
);

export default ThemeManager;
