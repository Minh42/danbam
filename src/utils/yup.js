import * as yup from 'yup';

const signinFormSchema = yup.object({
  email: yup
    .string()
    .email('Adresse e-mail incorrecte.')
    .required("La saisie d'une adresse e-mail est requise."),
  password: yup.string().required("La saisie d'un mot de passe est requise."),
});

const signupFormSchema = yup.object({
  email: yup
    .string()
    .email('Adresse e-mail incorrecte.')
    .required("La saisie d'une adresse e-mail est requise."),
  password: yup
    .string()
    .required("La saisie d'un mot de passe est requise.")
    .matches(
      /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/,
      'Ton mot de passe doit contenir au moins une majuscule, une minuscule, un chiffre, un caractère spécial et un minimum de 8 caractères.',
    ),
});

const forgotFormSchema = yup.object({
  email: yup
    .string()
    .email('Adresse e-mail incorrecte.')
    .required("La saisie d'une adresse e-mail est requise."),
});

export { signinFormSchema, signupFormSchema, forgotFormSchema };
