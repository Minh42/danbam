import React, { createContext, useState } from 'react';

export const FloatingButtonContext = createContext();

export const FloatingButtonProvider = ({ children }) => {
  const [showFloatingButton, setShowFloatingButton] = useState(false);
  return (
    <FloatingButtonContext.Provider value={{ showFloatingButton, setShowFloatingButton }}>
      {children}
    </FloatingButtonContext.Provider>
  );
};
