import React, { useState, useEffect, useContext, createContext } from 'react';
import { StatusBar } from 'react-native';
import { ThemeProvider } from 'styled-components/native';
import { Appearance } from 'react-native-appearance';
import lightTheme from '../themes/lightTheme';
import darkTheme from '../themes/darkTheme';

const defaultMode = Appearance.getColorScheme() || 'light';

const ThemeContext = createContext({
  mode: defaultMode,
  setMode: (mode) => console.log(mode),
});

const useTheme = () => useContext(ThemeContext);

const ManageThemeProvider = ({ children }) => {
  const [themeState, setThemeState] = useState(defaultMode);
  const setMode = (mode) => {
    setThemeState(mode);
  };
  useEffect(() => {
    const subscription = Appearance.addChangeListener(({ colorScheme }) => {
      setThemeState(colorScheme);
    });
    return () => subscription.remove();
  }, []);
  return (
    <ThemeContext.Provider value={{ mode: themeState, setMode }}>
      <ThemeProvider theme={themeState === 'dark' ? darkTheme.theme : lightTheme.theme}>
        <>
          <StatusBar barStyle={themeState === 'dark' ? 'dark-content' : 'light-content'} />
          {children}
        </>
      </ThemeProvider>
    </ThemeContext.Provider>
  );
};

export { useTheme, ManageThemeProvider };
