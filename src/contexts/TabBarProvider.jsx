import React, { createContext, useState } from 'react';

export const TabBarContext = createContext();

export const TabBarProvider = ({ children }) => {
  const [showTabBar, setShowTabBar] = useState(true);
  return (
    <TabBarContext.Provider value={{ showTabBar, setShowTabBar }}>
      {children}
    </TabBarContext.Provider>
  );
};
