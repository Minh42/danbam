import React, { createContext, useState } from 'react';
import { Alert } from 'react-native';
import firebase from 'firebase/app';

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(null);

  return (
    <AuthContext.Provider
      value={{
        user,
        setUser,
        signup: async (email, password) => {
          try {
            await firebase.auth().createUserWithEmailAndPassword(email, password);
            await firebase.auth().currentUser.sendEmailVerification();
            await firebase
              .firestore()
              .collection('users')
              .doc(firebase.auth().currentUser.uid)
              .set({
                firstName: null,
                lastName: null,
                email: email,
                userImg: null,
                createdAt: firebase.firestore.Timestamp.fromDate(new Date()),
              });
            Alert.alert(
              'Message de success',
              'Merci de bien vouloir valider ton compte par e-mail.',
            );
          } catch (err) {
            Alert.alert(
              "Message d'erreur",
              'Un compte existe déjà avec cette adresse e-mail. Merci de réessayer.',
            );
          }
        },
        signin: async (email, password) => {
          try {
            await firebase.auth().signInWithEmailAndPassword(email, password);
            if (!firebase.auth().currentUser.emailVerified) {
              Alert.alert(
                "Message d'erreur",
                'Merci de bien vouloir valider ton compte par e-mail.',
              );
            }
          } catch (err) {
            Alert.alert(
              "Message d'erreur",
              'Compte inexistant ou mot de passe invalide. Merci de réessayer.',
            );
          }
        },
        signout: async () => {
          try {
            await firebase.auth().signOut();
          } catch (err) {
            console.log(err);
          }
        },
        forgotPassword: async (email) => {
          try {
            await firebase.auth().sendPasswordResetEmail(email);
            Alert.alert(
              'Message de success',
              'Merci de bien vouloir réinitialiser ton mot de passe avec le lien envoyé par e-mail.',
            );
          } catch (err) {
            Alert.alert(
              "Message d'erreur",
              'Compte inexistant ou adresse e-mail invalide. Merci de réessayer.',
            );
          }
        },
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

/*
import * as Facebook from 'expo-facebook';
import { FACEBOOK_APP_ID, FACEBOOK_APP_NAME } from '@env';

        signinWithFacebook: async () => {
          await Facebook.initializeAsync({ appId: FACEBOOK_APP_ID, appName: FACEBOOK_APP_NAME });
          const { type, token } = await Facebook.logInWithReadPermissionsAsync({
            permissions: ['public_profile', 'email'],
          });
          if (type == 'success') {
            const credential = firebase.auth.FacebookAuthProvider.credential(token);
            firebase.auth().signInWithCredential(credential);
          }
        },



*/
