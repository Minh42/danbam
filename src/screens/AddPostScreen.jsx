import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { View, Button, Platform, Image } from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import * as ImagePicker from 'expo-image-picker';
import firebase from '../services/firebase';

const AddPostScreen = () => {
  const [image, setImage] = useState(null);

  useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button title="Pick an image from camera roll" onPress={pickImage} />
      {image && <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
    </View>
  );
};

export default AddPostScreen;

/*
import React, { useState, useContext } from 'react';
import ImagePicker from 'react-native-image-crop-picker';
import styled from 'styled-components';
import firebase from '../services/firebase';
import {
  ActivityIndicator,
  Button,
  Platform,
  SafeAreaView,
  Modal,
  TouchableWithoutFeedback,
} from 'react-native';
import { AuthContext } from '../contexts/AuthProvider';
import TextPicker from '../components/TextPicker';
import { BodyTextRegularMedium, TextInput } from '../atoms/Typography';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import PostActionsMenu from '../components/PostActionsMenu';
import { TabBarContext } from '../contexts/TabBarProvider';

const Container = styled.View`
  flex: 1;
  background-color: ${(props) => props.theme.backgroundColor};
  justify-content: space-between;
  background-color: white;
`;

const Icon = styled(MaterialIcon)`
  color: ${(props) => props.theme.textColor.high};
`;

const Image = styled.Image`
  width: 100%;
  height: 250px;
`;

const InputCommunityName = styled.TouchableOpacity`
  border-bottom-width: 1px;
  border-bottom-color: ${(props) => props.theme.primaryLightColor};
  flex-direction: row;
  align-items: center;
  height: 48px;
`;

const InputPostTitle = styled.View`
  border-bottom-width: 1px;
  border-bottom-color: ${(props) => props.theme.primaryLightColor};
  flex-direction: row;
  align-items: center;
  height: 48px;
`;

const InputPostContent = styled.View`
  height: 48px;
`;

const AddPostScreen = () => {
  const { user } = useContext(AuthContext);
  const [title, setTitle] = useState(null);
  const [image, setImage] = useState(null);
  const [post, setPost] = useState(null);
  const [uploading, setUploading] = useState(false);
  const [transferred, setTransferred] = useState(0);
  const [isModalVisible, setModalVisible] = useState(false);

  const { setShowTabBar } = useContext(TabBarContext);
  setShowTabBar(false);

  const takePhotoFromCamera = () => {
    ImagePicker.openCamera({
      width: 1200,
      height: 780,
      cropping: true,
    }).then((image) => {
      const imageUri = Platform.OS === 'ios' ? image.sourceURL : image.path;
      setImage(imageUri);
    });
  };

  const choosePhotoFromLibrary = () => {
    ImagePicker.openPicker({
      width: 1200,
      height: 780,
      cropping: true,
    }).then((image) => {
      const imageUri = Platform.OS === 'ios' ? image.sourceURL : image.path;
      setImage(imageUri);
    });
  };

  const uploadImage = async () => {
    const uploadUri = image;
    let filename = uploadUri.substring(uploadUri.lastIndexof('/') + 1);

    // Add timestamp to File Name
    const extension = filename.split('.').pop();
    const name = filename.split('.').slice(0, -1).join('.');
    filename = name + Date.now() + '.' + extension;

    setUploading(true);
    setTransferred(0);
    const storageRef = firebase.storage().ref(`photos/${filename}`);
    const task = storageRef.putFile(pathToFile);
    task.on('state_changed', (taskSnapshot) => {
      console.log(`${taskSnapshot.bytesTransferred} transferred out of ${taskSnapshot.totalBytes}`);
      setTransferred(Math.round((taskSnapshot.bytesTransferred / taskSnapshot.totalBytes) * 100));
    });
    try {
      await task;
      const url = await storageRef.getDownloadURL();
      setUploading(false);
      setImage(null);
      return url;
    } catch (err) {
      console.log(err);
      return null;
    }
  };

  return (
    <Container>
      <SafeAreaView>
        <InputCommunityName onPress={() => setModalVisible(true)}>
          <BodyTextRegularMedium style={{ marginLeft: 16, marginRight: 16 }}>
            Choisis une communauté*
          </BodyTextRegularMedium>
          <Icon name="expand-more" size={27}></Icon>
        </InputCommunityName>
        <InputPostTitle>
          <TextInput
            placeholder="Titre du post* (max. 30 caractères)"
            autoCapitalize="none"
            value={title}
            onChangeText={(content) => setTitle(content)}
            maxLength={30}
            style={{ marginLeft: 16, marginRight: 16 }}
          />
        </InputPostTitle>
        {image != null ? <Image source={{ uri: image }} /> : null}
        {uploading ? <ActivityIndicator size="large" /> : null}
        <InputPostContent>
          <TextInput
            placeholder="Contenu du post"
            multiline
            onChangeText={(content) => setPost(content)}
            value={post}
            style={{ marginLeft: 16, marginRight: 16, marginTop: 8 }}
          />
        </InputPostContent>
        <PostActionsMenu
          takePhotoFromCamera={takePhotoFromCamera}
          choosePhotoFromLibrary={choosePhotoFromLibrary}
        />
      </SafeAreaView>
    </Container>
  );
};

export default AddPostScreen;

/*

  const addPost = async () => {
    try {
      const imageUrl = await uploadImage();
      firebase
        .firestore()
        .collection('posts')
        .add({
          userId: user.uid,
          post: post,
          postImg: imageUrl,
          postTime: firebase.firestore.Timestamp.fromDate(new Date()),
          likes: null,
          comments: null,
        });
      setPost(null);
    } catch (err) {
      throw err;
    }
  };

           
        <PostActionsMenu
          takePhotoFromCamera={takePhotoFromCamera}
          choosePhotoFromLibrary={choosePhotoFromLibrary}
          addPost={addPost}
        />
 



                <Modal
          animationType="slide"
          transparent={true}
          visible={isModalVisible}
          onRequestClose={() => {
            setModalVisible(!isModalVisible);
          }}
        >
          <TouchableWithoutFeedback onPress={() => setModalVisible(!isModalVisible)}>
            <TextPicker />
          </TouchableWithoutFeedback>
        </Modal>
        <Button title="Upload Picture" onPress={() => takePhotoFromCamera} />

*/
