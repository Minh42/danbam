import React from 'react';
import styled from 'styled-components';
import * as Animatable from 'react-native-animatable';
import { H1, BodyTextRegularSmall } from '../atoms/Typography';
import SigninForm from '../components/SigninForm';

const Container = styled.View`
  flex: 1;
  background-color: ${(props) => props.theme.backgroundColor};
`;

const Header = styled.View`
  flex: 1;
  justify-content: flex-end;
  padding-vertical: ${(props) => props.theme.spacing.xxl};
  padding-horizontal: ${(props) => props.theme.spacing.lg};
`;

const Footer = styled.View`
  flex: 4;
  background-color: ${(props) => props.theme.surfaceColor};
  border-top-left-radius: ${(props) => props.theme.borderRadius.lg};
  border-top-right-radius: ${(props) => props.theme.borderRadius.lg};
  padding-vertical: ${(props) => props.theme.spacing.xl};
  padding-horizontal: ${(props) => props.theme.spacing.lg};
`;

const TextWrapper = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-top: ${(props) => props.theme.spacing.xl};
`;

const AnimatedFooter = Animatable.createAnimatableComponent(Footer);

const SigninScreen = ({ navigation }) => {
  return (
    <Container>
      <Header>
        <H1 altColor="true">Bienvenue !</H1>
      </Header>
      <AnimatedFooter animation="fadeInUpBig">
        <SigninForm navigation={navigation} />
        <TextWrapper>
          <BodyTextRegularSmall style={{ marginRight: 4 }}>
            Pas encore de compte ?
          </BodyTextRegularSmall>
          <BodyTextRegularSmall
            onPress={() => navigation.navigate('Signup')}
            textUnderlined="underline"
          >
            Inscris-toi
          </BodyTextRegularSmall>
        </TextWrapper>
      </AnimatedFooter>
    </Container>
  );
};

export default SigninScreen;

/*
<DividerWithText text="ou connecte toi avec" />
<SocialOauth />
*/
