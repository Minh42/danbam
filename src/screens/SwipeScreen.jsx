import React from 'react';
import { SafeAreaView } from 'react-native';
import styled from 'styled-components';
import Deck from '../components/Deck';
import { primaryColor, whiteColor } from '../constants/Colors';
import { H4 } from '../atoms/Typography';
import Wave from '../atoms/Wave';

const data = [
  {
    id: 1,
    text: 'Card #1',
    uri: 'https://picsum.photos/200/300',
    userName: 'Kkui',
    firstName: 'Minh',
    lastName: 'Pham',
  },
  {
    id: 2,
    text: 'Card #2',
    uri: 'https://picsum.photos/200/300',
    username: 'Pad',
  },
  {
    id: 3,
    text: 'Card #3',
    uri: 'https://picsum.photos/200/300',
    username: 'Luka',
  },
  {
    id: 4,
    text: 'Card #4',
    uri: 'https://picsum.photos/200/300',
    username: 'Kkiki',
  },
];

const Container = styled.View`
  flex: 1;
  background-color: ${primaryColor};
`;

const SwipeScreen = ({ navigation }) => {
  return (
    <Container>
      <SafeAreaView>
        <H4
          textColor={whiteColor}
          textAlign="center"
          style={{ marginTop: 24, marginBottom: 24, marginRight: 32, marginLeft: 32 }}
        >
          Découvre 5 membres de notre communauté tous les jours
        </H4>
        <Deck data={data} />
        <Wave />
      </SafeAreaView>
    </Container>
  );
};

export default SwipeScreen;
