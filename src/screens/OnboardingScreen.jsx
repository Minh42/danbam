import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import Onboarding from 'react-native-onboarding-swiper';
import OnboardingImg2 from '../assets/images/onboarding-img2.jpeg';
import OnboardingImg3 from '../assets/images/onboarding-img3.jpeg';
import OnboardingImg4 from '../assets/images/onboarding-img4.jpeg';
import { primaryColor, blackColor, whiteColor } from '../constants/Colors';
import styled from 'styled-components';
import { H1, BodyTextRegularMedium } from '../atoms/Typography';
import Icon from 'react-native-vector-icons/MaterialIcons';

const SkipButton = ({ ...props }) => {
  return (
    <BodyTextRegularMedium {...props} style={{ marginLeft: 8 }}>
      Passer
    </BodyTextRegularMedium>
  );
};

const NextButton = ({ ...props }) => {
  return (
    <BodyTextRegularMedium {...props} style={{ marginRight: 8 }}>
      Suivant
    </BodyTextRegularMedium>
  );
};

const DoneButton = ({ ...props }) => {
  return (
    <TouchableOpacity {...props}>
      <Icon name="done" size={22} style={{ marginRight: 16 }} />
    </TouchableOpacity>
  );
};

const Image = styled.Image`
  width: 350px;
  height: 350px;
  border-radius: 200px;
`;

const DotButton = ({ selected }) => {
  const backgroundColor = selected ? primaryColor : blackColor;
  return (
    <View
      style={{
        width: 8,
        height: 8,
        marginHorizontal: 3,
        backgroundColor,
        borderRadius: 100,
      }}
    />
  );
};

const OnboardingScreen = ({ navigation }) => {
  return (
    <Onboarding
      SkipButtonComponent={SkipButton}
      NextButtonComponent={NextButton}
      DoneButtonComponent={DoneButton}
      DotComponent={DotButton}
      onSkip={() => navigation.replace('Signin')}
      onDone={() => navigation.navigate('Signin')}
      pages={[
        {
          backgroundColor: whiteColor,
          image: <Image source={OnboardingImg2} />,
          title: (
            <H1 textAlign="center" style={{ marginLeft: 24, marginRight: 24, marginBottom: 8 }}>
              La 1ère communauté Culture & Asie
            </H1>
          ),
          subtitle: (
            <BodyTextRegularMedium textAlign="center" style={{ marginLeft: 24, marginRight: 24 }}>
              Rencontres, voyages, évènements… toute la culture asiatique sur ton mobile.
            </BodyTextRegularMedium>
          ),
        },
        {
          backgroundColor: whiteColor,
          image: <Image source={OnboardingImg2} />,
          title: (
            <H1 textAlign="center" style={{ marginLeft: 24, marginRight: 24, marginBottom: 8 }}>
              Évènements, actualités et bons plans !
            </H1>
          ),
          subtitle: (
            <BodyTextRegularMedium textAlign="center" style={{ marginLeft: 24, marginRight: 24 }}>
              Toute l’actualité et évènements sur l’Asie autour de chez toi.
            </BodyTextRegularMedium>
          ),
        },
        {
          backgroundColor: whiteColor,
          image: <Image source={OnboardingImg3} />,
          title: (
            <H1 textAlign="center" style={{ marginLeft: 24, marginRight: 24, marginBottom: 8 }}>
              Fais des rencontres partout dans le monde !
            </H1>
          ),
          subtitle: (
            <BodyTextRegularMedium textAlign="center" style={{ marginLeft: 24, marginRight: 24 }}>
              Rencontre de nouvelles personnes qui partagent les mêmes intérêts que toi.
            </BodyTextRegularMedium>
          ),
        },
        {
          backgroundColor: whiteColor,
          image: <Image source={OnboardingImg4} />,
          title: (
            <H1 textAlign="center" style={{ marginLeft: 24, marginRight: 24, marginBottom: 8 }}>
              안녕하세요. おはようございます. 你好.
            </H1>
          ),
          subtitle: (
            <BodyTextRegularMedium textAlign="center" style={{ marginLeft: 24, marginRight: 24 }}>
              Apprends le coréen, japonais ou chinois avec les membres de notre communauté.
            </BodyTextRegularMedium>
          ),
        },
      ]}
    />
  );
};

export default OnboardingScreen;
