import React, { useState, useContext, useEffect } from 'react';
import styled from 'styled-components';
import { FlatList, Modal, Button, TouchableOpacity } from 'react-native';
import Divider from '../atoms/Divider';
import ActivityIndicator from '../components/ActivityIndicator';
import { AuthContext } from '../contexts/AuthProvider';
import firebase from '../services/firebase';
import { whiteColor } from '../constants/Colors';
import ChatListItem from '../components/ChatListItem';
import FloatingButton from '../atoms/FloatingButton';
import Contacts from '../components/Contacts';
import { H1 } from '../atoms/Typography';

const Container = styled.View`
  flex: 1;
  background-color: ${whiteColor};
`;

const Text = styled.Text`
  color: ${({ theme: { colors } }) => colors.persianGreen};
`;

const MessagesScreen = ({ navigation }) => {
  const [chatRooms, setChatRooms] = useState([]);
  const [loading, setLoading] = useState(true);
  const { user } = useContext(AuthContext);
  const [modalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    const unsubscribe = firebase
      .firestore()
      .collection('chats')
      .where('members', 'array-contains', user.uid)
      .orderBy('createdAt', 'desc')
      .onSnapshot((querySnapshot) => {
        const chatRooms = querySnapshot.docs.map((documentSnapshot) => {
          return {
            _id: documentSnapshot.id,
            name: '',
            ...documentSnapshot.data(),
          };
        });
        setChatRooms(chatRooms);

        if (loading) {
          setLoading(false);
        }
      });
    return () => unsubscribe();
  }, []);

  /*
  if (loading) {
    return <ActivityIndicator />;
  }
  */

  return (
    <Container>
      <H1>HELLO WORLD</H1>
      <FlatList
        data={chatRooms}
        keyExtractor={(item) => item._id}
        ItemSeparatorComponent={() => <Divider marginTop={16} />}
        renderItem={({ item }) => {
          return <ChatListItem item={item} navigation={navigation} />;
        }}
      />
      <TouchableOpacity onPress={() => setModalVisible(true)}>
        <FloatingButton name="send" />
      </TouchableOpacity>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <Contacts />
      </Modal>
    </Container>
  );
};

export default MessagesScreen;
