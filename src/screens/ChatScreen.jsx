import React, { useState, useEffect, useContext } from 'react';
import styled from 'styled-components';
import { FlatList } from 'react-native';
import { useRoute } from '@react-navigation/native';
import ChatMessage from '../components/ChatMessage';
import ChatInput from '../components/ChatInput';
import firebase from '../services/firebase';
import { AuthContext } from '../contexts/AuthProvider';
import { greyColor } from '../constants/Colors';

const Container = styled.View`
  flex: 1;
`;

const ChatScreen = ({ navigation }) => {
  const route = useRoute();
  const { user } = useContext(AuthContext);
  const { id } = route.params;

  const [messages, setMessages] = useState([]);

  useEffect(() => {
    const unsubscribe = firebase
      .firestore()
      .collection('chats')
      .doc(id)
      .collection('messages')
      .onSnapshot((querySnapshot) => {
        const messagesFirestore = querySnapshot
          .docChanges()
          .filter(({ type }) => type === 'added')
          .map(({ doc }) => {
            return {
              _id: doc.id,
              ...doc.data(),
              createdAt: doc.data().createdAt.toDate(),
            };
          })
          .sort((a, b) => b.createdAt.getTime() - a.createdAt.getTime());
        setMessages(messagesFirestore);
        /*
          appendMessages(messagesFirestore);
          */
      });
    return () => unsubscribe();
  }, []);

  /*
  const appendMessages = useCallback(
    (messages) => {
      setMessages((previousMessages) => GiftedChat.append(previousMessages, messages));
    },
    [messages],
  );
  */

  const sendMessage = () => {
    const text = 'Hello World';
    firebase
      .firestore()
      .collection('chats')
      .doc(id)
      .collection('messages')
      .add({
        text,
        createdAt: new Date(),
        user: {
          _id: user.uid,
        },
      });
  };

  return (
    <Container>
      <FlatList
        inverted
        data={messages}
        keyExtractor={(item) => item._id}
        renderItem={({ item }) => {
          return <ChatMessage item={item} />;
        }}
      />
      <ChatInput onPress={() => sendMessage()} />
    </Container>
  );
};

export default ChatScreen;
