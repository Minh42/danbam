import React from 'react';
import styled from 'styled-components';
import * as Animatable from 'react-native-animatable';
import Logo from '../assets/images/logo.png';
import ButtonWithIcon from '../atoms/ButtonWithIcon';
import { H1, BodyTextRegularSmall } from '../atoms/Typography';

const Container = styled.View`
  flex: 1;
  background-color: ${(props) => props.theme.backgroundColor};
`;

const Header = styled.View`
  flex: 2;
  justify-content: center;
  align-items: center;
`;

const Footer = styled.View`
  flex: 1;
  border-top-left-radius: ${(props) => props.theme.borderRadius.lg};
  border-top-right-radius: ${(props) => props.theme.borderRadius.lg};
  padding-vertical: ${(props) => props.theme.spacing.xxl};
  padding-horizontal: ${(props) => props.theme.spacing.xl};
  background-color: ${(props) => props.theme.surfaceColor};
`;

const Image = styled.Image`
  height: ${(props) => props.theme.window.height * 0.1}px;
  width: ${(props) => props.theme.window.width * 0.5}px;
`;

const ButtonWrapper = styled.View`
  align-items: flex-end;
  margin-top: ${(props) => props.theme.spacing.xl};
`;

const AnimatedImage = Animatable.createAnimatableComponent(Image);
const AnimatedFooter = Animatable.createAnimatableComponent(Footer);

const SplashScreen = ({ navigation }) => {
  return (
    <Container>
      <Header>
        <AnimatedImage source={Logo} animation="bounceIn" resizeMode="center" duration={1500} />
      </Header>
      <AnimatedFooter animation="fadeInUpBig">
        <H1 style={{ marginBottom: 8 }}>Rejoins nous sur Danbam !</H1>
        <BodyTextRegularSmall>Connecte toi avec ton compte</BodyTextRegularSmall>
        <ButtonWrapper>
          <ButtonWithIcon
            text="C'est parti !"
            name="chevron-right"
            onPress={() => navigation.navigate('Signin')}
          />
        </ButtonWrapper>
      </AnimatedFooter>
    </Container>
  );
};

export default SplashScreen;
