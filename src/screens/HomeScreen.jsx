import React, { useState, useEffect, useContext } from 'react';
import styled from 'styled-components';
import FloatingButton from '../atoms/FloatingButton';
import firebase from '../services/firebase';
import { FlatList } from 'react-native';
import Post from '../components/Post';
import UserActionsMenu from '../components/UserActionsMenu';
import { TouchableOpacity } from 'react-native';
import { Text } from 'react-native';
import AnimatedScrollView from '../components/AnimatedScrollView';
import { FloatingButtonContext } from '../contexts/FloatingButtonProvider';
import { Modal } from 'react-native';

const Container = styled.View`
  flex: 1;
`;

const HomeScreen = ({ navigation }) => {
  const [posts, setPosts] = useState(null);
  const [loading, setLoading] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);
  const { setShowFloatingButton } = useContext(FloatingButtonContext);

  const fetchPosts = async () => {
    try {
      const list = [];
      let querySnapshot = firebase.firestore().collection('posts').get();
      (await querySnapshot).forEach((doc) => {
        const { userId, post, postImg, likes, comments, createdAt } = doc.data();
        list.push({
          id: doc.id,
          userId,
          userName,
          userImg,
          post,
          postImg,
          liked: false,
          likes,
          comments,
          createdAt,
        });
      });
      setPosts(list);
      if (loading) setLoading(false);
    } catch (err) {
      throw err;
    }
  };

  const openBottomMenu = () => {
    setShowFloatingButton(false);
    setModalVisible(true);
  };

  const closeBottomMenu = () => {
    setModalVisible(false);
    setShowFloatingButton(true);
  };

  useEffect(() => {
    fetchPosts();
  }, []);

  return (
    <Container>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <UserActionsMenu closeBottomMenu={() => closeBottomMenu()} navigation={navigation} />
      </Modal>
      <AnimatedScrollView>
        <FlatList
          data={posts}
          renderItem={({ item }) => <Post item={item} />}
          keyExtractor={(item) => item.id}
          showsVerticalScrollIndicator={false}
        />
        <Text>
          "On the other hand, we denounce with righteous indignation and dislike men who are so
          beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire,
          that they cannot foresee the pain and trouble that are bound to ensue; and equal blame
          belongs to those who fail in their duty through weakness of will, which is the same as
          saying through shrinking from toil and pain. These cases are perfectly simple and easy to
          On the other hand, we denounce with righteous indignation and dislike men who are so
          beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire,
          that they cannot foresee the pain and trouble that are bound to ensue; and equal blame
          belongs to those who fail in their duty through weakness of will, which is the same as
          saying through shrinking from toil and pain. These cases are perfectly simple and easy to
          distinguish. In a free hour, when our power of choice is untrammelled and when nothing
          prevents our being able to do what we like best, every pleasure is to be welcomed and
          every pain avoided. But in certain circumstances and owing to the claims of duty or the
          obligations of business it will frequently occur that pleasures have to be repudiated and
          annoyances accepted. The wise man therefore always holds in these matters to this
          principle of selection: he rejects pleasures to secure other greater pleasures, or else he
          endures pains to avoid worse pains. On the other hand, we denounce with righteous
          indignation and dislike men who are so beguiled and demoralized by the charms of pleasure
          of the moment, so blinded by desire, that they cannot foresee the pain and trouble that
          are bound to ensue; and equal blame belongs to those who fail in their duty through
          weakness of will, which is the same as saying through shrinking from toil and pain. These
          cases are perfectly simple and easy to distinguish. In a free hour, when our power of
          choice is untrammelled and when nothing prevents our being able to do what we like best,
          every pleasure is to be welcomed and every pain avoided. But in certain circumstances and
          owing to the claims of duty or the obligations of business it will frequently occur that
          pleasures have to be repudiated and annoyances accepted. The wise man therefore always
          holds in these matters to this principle of selection: he rejects pleasures to secure
          other greater pleasures, or else he endures pains to avoid worse pains. On the other hand,
          we denounce with righteous indignation and dislike men who are so beguiled and demoralized
          by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee
          the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in
          their duty through weakness of will, which is the same as saying through shrinking from
          toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour,
          when our power of choice is untrammelled and when nothing prevents our being able to do
          what we like best, every pleasure is to be welcomed and every pain avoided. But in certain
          circumstances and owing to the claims of duty or the obligations of business it will
          frequently occur that pleasures have to be repudiated and annoyances accepted. The wise
          man therefore always holds in these matters to this principle of selection: he rejects
          pleasures to secure other greater pleasures, or else he endures pains to avoid worse
          pains. On the other hand, we denounce with righteous indignation and dislike men who are
          so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire,
          that they cannot foresee the pain and trouble that are bound to ensue; and equal blame
          belongs to those who fail in their duty through weakness of will, which is the same as
          saying through shrinking from toil and pain. These cases are perfectly simple and easy to
          distinguish. In a free hour, when our power of choice is untrammelled and when nothing
          prevents our being able to do what we like best, every pleasure is to be welcomed and
          every pain avoided. But in certain circumstances and owing to the claims of duty or the
          obligations of business it will frequently occur that pleasures have to be repudiated and
          annoyances accepted. The wise man therefore always holds in these matters to this
          principle of selection: he rejects pleasures to secure other greater pleasures, or else he
          endures pains to avoid worse pains. On the other hand, we denounce with righteous
          indignation and dislike men who are so beguiled and demoralized by the charms of pleasure
          of the moment, so blinded by desire, that they cannot foresee the pain and trouble that
          are bound to ensue; and equal blame belongs to those who fail in their duty through
          weakness of will, which is the same as saying through shrinking from toil and pain. These
          cases are perfectly simple and easy to distinguish. In a free hour, when our power of
          choice is untrammelled and when nothing prevents our being able to do what we like best,
          every pleasure is to be welcomed and every pain avoided. But in certain circumstances and
          owing to the claims of duty or the obligations of business it will frequently occur that
          pleasures have to be repudiated and annoyances accepted. The wise man therefore always
          holds in these matters to this principle of selection: he rejects pleasures to secure
          other greater pleasures, or else he endures pains to avoid worse pains. On the other hand,
          we denounce with righteous indignation and dislike men who are so beguiled and demoralized
          by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee
          the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in
          their duty through weakness of will, which is the same as saying through shrinking from
          toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour,
          when our power of choice is untrammelled and when nothing prevents our being able to do
          what we like best, every pleasure is to be welcomed and every pain avoided. But in certain
          circumstances and owing to the claims of duty or the obligations of business it will
          frequently occur that pleasures have to be repudiated and annoyances accepted. The wise
          man therefore always holds in these matters to this principle of selection: he rejects
          pleasures to secure other greater pleasures, or else he endures pains to avoid worse
          pains.
        </Text>
      </AnimatedScrollView>
      <TouchableOpacity onPress={() => openBottomMenu()}>
        <FloatingButton name="add" screen="AddPost" />
      </TouchableOpacity>
    </Container>
  );
};

export default HomeScreen;
