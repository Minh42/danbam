import React from 'react';
import { View, Text, Button } from 'react-native';
import FormButton from '../components/FormButton';

const AddRoomScreen = ({ navigation }) => {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Create a new chat room</Text>
      <Button title="Close Modal" onPress={() => navigation.goBack()} />
    </View>
  );
};
