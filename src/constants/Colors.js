const primaryColor = '#0000FF';
const secondaryColor = '#FF00FF';
const whiteColor = '#FFFFFF';
const blackColor = '#24262F';
const darkGreyColor1 = '#999';
const darkGreyColor2 = '#777';
const darkGreyColor3 = '#333';
const lightGreyColor1 = '#faf9f9';
const lightGreyColor2 = '#f4f2f2';
const lightGreyColor3 = '#f0eeee';
const redColor = '#FB3F07';
const greenColor = '#17D38A';
const primaryColorLight = '#E5E8FF';
const purpleColor = '#8338EC';
const turqoiseColor = '#17FFD8';
const yellowColor = '#FFBE0B';
const orangeColor = '#FB5607';

export {
  primaryColor,
  secondaryColor,
  whiteColor,
  blackColor,
  darkGreyColor1,
  darkGreyColor2,
  darkGreyColor3,
  lightGreyColor1,
  lightGreyColor2,
  lightGreyColor3,
  redColor,
  greenColor,
  primaryColorLight,
  purpleColor,
  turqoiseColor,
  yellowColor,
  orangeColor,
};
