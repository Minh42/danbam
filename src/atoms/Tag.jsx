import styled from 'styled-components';

const Tag = styled.View`
  height: 25px;
  padding-left: 16px;
  padding-right: 16px;
  border-radius: 25px;
  background-color: ${(props) => props.color || blackColor};
  justify-content: center;
  align-items: center;
  border: none;
`;

export default Tag;
