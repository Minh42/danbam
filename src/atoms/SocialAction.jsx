import React from 'react';
import styled from 'styled-components';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { BodyTextRegularSmall } from './Typography';

const Container = styled.View`
  flex-direction: row;
  align-items: center;
`;

const SocialAction = ({ name, count, hasMarginRight }) => {
  if (hasMarginRight) {
    return (
      <Container style={{ marginRight: 16 }}>
        <Icon name={name} size={27} style={{ marginRight: 8 }} />
        <BodyTextRegularSmall>{count}</BodyTextRegularSmall>
      </Container>
    );
  }
  return (
    <Container>
      <Icon name={name} size={27} style={{ marginRight: 8 }} />
      <BodyTextRegularSmall>{count}</BodyTextRegularSmall>
    </Container>
  );
};

export default SocialAction;
