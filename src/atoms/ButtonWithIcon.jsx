import React from 'react';
import { TouchableOpacity } from 'react-native';
import styled from 'styled-components';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { BodyTextMediumMedium } from './Typography';

const Container = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  border-radius: ${(props) => props.theme.borderRadius.sm};
  background-color: ${(props) => props.theme.primaryColor};
  padding-vertical: ${(props) => props.theme.spacing.md};
  padding-horizontal: ${(props) => props.theme.spacing.xl};
`;

const Icon = styled(MaterialIcon)`
  color: ${(props) => props.theme.textColorAlt.high};
`;

const ButtonWithIcon = ({ text, name, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Container>
        <BodyTextMediumMedium altColor="true" textAlign="center" style={{ marginRight: 8 }}>
          {text}
        </BodyTextMediumMedium>
        <Icon name={name} size={17} />
      </Container>
    </TouchableOpacity>
  );
};

export default ButtonWithIcon;
