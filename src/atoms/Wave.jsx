import React from 'react';
import styled from 'styled-components';
import Svg, { Path, G, Rect } from 'react-native-svg';
import Layout from '../constants/Layout';

const Container = styled.View`
  z-index: -1;
  flex: 1;
  align-items: center;
  top: ${Layout.window.height / 3}px;
`;

const Wave = () => {
  return (
    <Container>
      <Svg
        xmlns="http://www.w3.org/2000/svg"
        width="888.556"
        height="483.811"
        viewBox="0 0 888.556 483.811"
      >
        <G id="VAGUES_BLEUES" data-name="VAGUES BLEUES" transform="translate(680.198 -784)">
          <Path
            id="Tracé_186"
            data-name="Tracé 186"
            d="M-54.1,6827.365s19.651,16.5,61.9,11.4,80.729-76.637,123.183-69.865,23.57-29.181,86.251-20.1,90.91,33.083,115.341,41.649,21.139,13.65,105.388-27.82,244.478-58.569,244.478-58.569v149.275H-58.984Z"
            transform="translate(-621.213 -5920.063)"
            fill="#fff"
            opacity="0.3"
          />
          <Path
            id="Tracé_187"
            data-name="Tracé 187"
            d="M-55.292,6804.753s14.869,13.476,46.842,9.309,61.088-62.583,93.213-57.053,17.836-23.83,65.266-16.413,68.791,27.017,87.278,34.012,16,11.147,79.747-22.718,185-47.828,185-47.828v121.9H-58.984Z"
            transform="translate(-466.261 -5906.031)"
            fill="#1934ff"
          />
          <Path
            id="Tracé_188"
            data-name="Tracé 188"
            d="M-53.316,6854.408s22.824,20.122,71.9,13.9,93.765-93.445,143.074-85.187,27.376-35.582,100.179-24.506S367.424,6798.953,395.8,6809.4s24.552,16.644,122.405-33.922,283.954-71.414,283.954-71.414v182.014H-58.984Z"
            transform="translate(-594.802 -5914.285)"
            fill="none"
            stroke="#fff"
            stroke-width="2"
            opacity="0.3"
          />
          <Rect
            id="Rectangle_287"
            data-name="Rectangle 287"
            width="528"
            height="355"
            transform="translate(-493.244 912.811)"
            fill="#1934ff"
          />
        </G>
      </Svg>
    </Container>
  );
};

export default Wave;
