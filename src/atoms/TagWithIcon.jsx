import React from 'react';
import styled from 'styled-components';
import { H3 } from './Typography';
import Icon from 'react-native-vector-icons/MaterialIcons';

const Container = styled.View`
  position: absolute;
  top: 30px;
  height: 29px;
  padding-left: 16px;
  padding-right: 16px;
  background-color: ${(props) => props.color};
  border-radius: 25px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  right: 21px;
`;

const TagWithIcon = ({ name, text, color }) => {
  return (
    <Container color={color}>
      <H3 style={{ marginRight: 8 }}>{text}</H3>
      <Icon name={name} size={27} />
    </Container>
  );
};

export default TagWithIcon;
