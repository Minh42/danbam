import React from 'react';
import { TouchableOpacity } from 'react-native';
import styled from 'styled-components';
import { BodyTextMediumMedium } from './Typography';

const Container = styled.View`
  border-radius: ${(props) => props.theme.borderRadius.sm};
  background-color: ${(props) => props.theme.primaryColor};
  padding-vertical: ${(props) => props.theme.spacing.md};
  padding-horizontal: ${(props) => props.theme.spacing.xl};
`;

const Button = ({ text, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Container>
        <BodyTextMediumMedium altColor="true" textAlign="center">
          {text}
        </BodyTextMediumMedium>
      </Container>
    </TouchableOpacity>
  );
};

export default Button;
