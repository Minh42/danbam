import React, { useContext, useRef, useEffect } from 'react';
import styled from 'styled-components';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { FloatingButtonContext } from '../contexts/FloatingButtonProvider';
import { Animated } from 'react-native';

const Container = styled.View`
  position: absolute;
  right: 24px;
  bottom: 32px;
  height: 60px;
  width: 60px;
  justify-content: center;
  align-items: center;
  border-radius: ${(props) => props.theme.borderRadius.xxl};
  background-color: ${(props) => props.theme.surfaceColor};
`;

const Icon = styled(MaterialIcon)`
  color: ${(props) => props.theme.textColor.high};
`;

const FloatingButton = ({ name }) => {
  const { showFloatingButton } = useContext(FloatingButtonContext);
  const animation = useRef(new Animated.Value(0)).current;

  const toggleFloatingButton = () => {
    if (showFloatingButton) {
      Animated.timing(animation, {
        toValue: 1,
        duration: 200,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(animation, {
        toValue: 0,
        duration: 200,
        useNativeDriver: true,
      }).start();
    }
  };

  useEffect(() => {
    toggleFloatingButton();
  }, [showFloatingButton]);

  return (
    <Container as={Animated.View} style={{ opacity: animation }}>
      <Icon name={name} size={27} />
    </Container>
  );
};

export default FloatingButton;
