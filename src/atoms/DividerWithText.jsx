import React from 'react';
import styled from 'styled-components';
import { BodyTextRegularSmall } from './Typography';

const Container = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-top: ${(props) => props.theme.spacing.xl};
  margin-bottom: ${(props) => props.theme.spacing.xl};
`;

const Divider = styled.View`
  border: 0.5px;
  border-color: ${(props) => props.theme.borderColor};
  width: 100px;
`;

const DividerWithText = ({ text }) => {
  return (
    <Container>
      <Divider />
      <BodyTextRegularSmall>{text}</BodyTextRegularSmall>
      <Divider />
    </Container>
  );
};

export default DividerWithText;
