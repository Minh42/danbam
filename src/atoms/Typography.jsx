import styled from 'styled-components';

const H1 = styled.Text`
  font-family: ${(props) => props.theme.typography.bold};
  font-size: 30px;
  color: ${(props) =>
    props.altColor ? props.theme.textColorAlt.high : props.theme.textColor.high};
  text-align: ${(props) => props.textAlign || 'left'};
`;

const H2 = styled.Text`
  font-family: ${(props) => props.theme.typography.bold};
  font-size: 25px;
  color: ${(props) =>
    props.altColor ? props.theme.textColorAlt.high : props.theme.textColor.high};
  text-align: ${(props) => props.textAlign || 'left'};
`;

const H3 = styled.Text`
  font-family: ${(props) => props.theme.typography.bold};
  font-size: 20px;
  color: ${(props) =>
    props.altColor ? props.theme.textColorAlt.high : props.theme.textColor.high};
  text-align: ${(props) => props.textAlign || 'left'};
`;

const H4 = styled.Text`
  font-family: ${(props) => props.theme.typography.bold};
  font-size: 17px;
  color: ${(props) =>
    props.altColor ? props.theme.textColorAlt.high : props.theme.textColor.high};
  text-align: ${(props) => props.textAlign || 'left'};
`;

const H5 = styled.Text`
  font-family: ${(props) => props.theme.typography.bold};
  font-size: 15px;
  color: ${(props) =>
    props.altColor ? props.theme.textColorAlt.high : props.theme.textColor.high};
  text-align: ${(props) => props.textAlign || 'left'};
`;

const H6 = styled.Text`
  font-family: ${(props) => props.theme.typography.bold};
  font-size: 13px;
  color: ${(props) =>
    props.altColor ? props.theme.textColorAlt.high : props.theme.textColor.high};
  text-align: ${(props) => props.textAlign || 'left'};
`;

const BodyTextMediumMedium = styled.Text`
  font-family: ${(props) => props.theme.typography.medium};
  font-size: 17px;
  color: ${(props) =>
    props.altColor ? props.theme.textColorAlt.high : props.theme.textColor.high};
  text-align: ${(props) => props.textAlign || 'left'};
`;

const BodyTextMediumSmall = styled.Text`
  font-family: ${(props) => props.theme.typography.medium};
  font-size: 15px;
  color: ${(props) =>
    props.altColor ? props.theme.textColorAlt.high : props.theme.textColor.high};
  text-align: ${(props) => props.textAlign || 'left'};
`;

const BodyTextRegularLarge = styled.Text`
  font-family: ${(props) => props.theme.typography.regular};
  font-size: 20px;
  color: ${(props) => (props.altColor ? props.theme.textColorAlt.low : props.theme.textColor.low)};
  text-align: ${(props) => props.textAlign || 'left'};
`;

const BodyTextRegularMedium = styled.Text`
  font-family: ${(props) => props.theme.typography.regular};
  font-size: 17px;
  color: ${(props) => (props.altColor ? props.theme.textColorAlt.low : props.theme.textColor.low)};
  text-align: ${(props) => props.textAlign || 'left'};
`;

const BodyTextRegularSmall = styled.Text`
  font-family: ${(props) => props.theme.typography.regular};
  font-size: 15px;
  color: ${(props) => (props.altColor ? props.theme.textColorAlt.low : props.theme.textColor.low)};
  text-align: ${(props) => props.textAlign || 'left'};
  text-decoration: ${(props) => props.textUnderlined || 'none'};
  text-decoration-color: ${(props) => props.theme.underlinedColor};
`;

const BodyTextRegularExtraSmall = styled.Text`
  font-family: ${(props) => props.theme.typography.regular};
  font-size: 12px;
  color: ${(props) => (props.altColor ? props.theme.textColorAlt.low : props.theme.textColor.low)};
  text-align: ${(props) => props.textAlign || 'left'};
`;

const TextInput = styled.TextInput.attrs((props) => ({
  placeholderTextColor: props.altColor ? props.theme.textColorAlt.low : props.theme.textColor.low,
}))`
  font-family: ${(props) => props.theme.typography.regular};
  font-size: 17px;
  color: ${(props) => (props.altColor ? props.theme.textColorAlt.low : props.theme.textColor.low)};
  text-align: ${(props) => props.textAlign || 'left'};
`;

const ErrorMessage = styled.Text`
  font-family: ${(props) => props.theme.typography.regular};
  font-size: 12px;
  color: ${(props) => props.theme.error};
  text-align: ${(props) => props.textAlign || 'left'};
`;

const SuccessMessage = styled.Text`
  font-family: ${(props) => props.theme.typography.regular};
  font-size: 12px;
  color: ${(props) => props.theme.success};
  text-align: ${(props) => props.textAlign || 'left'};
`;

export {
  H1,
  H2,
  H3,
  H4,
  H5,
  H6,
  BodyTextMediumMedium,
  BodyTextMediumSmall,
  BodyTextRegularLarge,
  BodyTextRegularMedium,
  BodyTextRegularSmall,
  BodyTextRegularExtraSmall,
  TextInput,
  ErrorMessage,
  SuccessMessage,
};
