import styled from 'styled-components';
import { primaryColorLight } from '../constants/Colors';

const Divider = styled.View`
  border: 0.5px;
  border-color: ${primaryColorLight};
  margin-top: ${(props) => props.marginTop | '32px'}
  margin-bottom: ${(props) => props.marginBottom | '32px'}
`;

export default Divider;
