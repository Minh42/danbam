import styled from 'styled-components';

const Avatar = styled.Image`
  ${({ small, medium, large }) => {
    switch (true) {
      case large:
        return 'width: 75px';
      case medium:
        return 'width: 53px';
      case small:
        return 'width: 38px';
      default:
        return 'width: 38px';
    }
  }}
  ${({ small, medium, large }) => {
    switch (true) {
      case large:
        return 'height: 75px';
      case medium:
        return 'height: 53px';
      case small:
        return 'height: 38px';
      default:
        return 'height: 38px';
    }
  }}
  border-radius: 100px;
`;

export default Avatar;
