import React, { useState } from 'react';
import RootNavigation from './src/navigation';
import * as Font from 'expo-font';
import AppLoading from 'expo-app-loading';
import ThemeManager from './src/themes';

const App = () => {
  const [isReady, setReady] = useState(false);

  const loadResourcesAsync = async () => {
    return await Promise.all([
      Font.loadAsync({
        'Montserrat-bold': require('./src/assets/fonts/Montserrat-Bold.ttf'),
        'Montserrat-medium': require('./src/assets/fonts/Montserrat-Medium.ttf'),
        'Montserrat-regular': require('./src/assets/fonts/Montserrat-Regular.ttf'),
      }),
    ]);
  };

  const handleLoadingError = () => {
    // Any error handling can be done here
  };

  const handleFinishLoading = () => {
    setReady(true);
  };

  if (!isReady) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={handleFinishLoading}
      />
    );
  }
  return (
    <ThemeManager>
      <RootNavigation />
    </ThemeManager>
  );
};

export default App;
